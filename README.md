<div align="center">
  <img src=".gitlab/logo.svg" height="80px"/><br/>
  <h1>Lab 3</h1>
  <p>VM Performance</a></p>
</div>

## Task 1

1. Create an instance:

   Reference: [link](https://multipass.run/docs/create-an-instance#heading--create-an-instance-with-custom-cpu-number-disk-and-ram)

   ```bash
   vova@vova-VB:~$ multipass launch --cpus 4 --disk 20G --mem 8G
   ```

   ![](.gitlab/img1.png)

   ![](.gitlab/img2.png)

2. Install apache server in the VM:

   Reference: [link](https://multipass.run/docs/linux-tutorial)

   ```bash
   vova@vova-VB:~$ multipass shell copious-alligator
   ubuntu@copious-alligator:~$ sudo apt update
   ubuntu@copious-alligator:~$ sudo apt install apache2
   ```

   ![](.gitlab/img3.png)

3. Activate graphical interface

   Reference: [link](https://multipass.run/docs/set-up-a-graphical-interface#heading--x11-on-linux)

   ```bash
   ubuntu@copious-alligator:~$ sudo apt install ubuntu-desktop xrdp
   ubuntu@copious-alligator:~$ sudo passwd ubuntu
   ubuntu@copious-alligator:~$ multipass list
   ```

   ![](.gitlab/img5.png)

4. Access the VM logs using multipass

   ```bash
   vova@vova-VB:~$ journalctl --unit 'snap.multipass*'  # access logs of multipass
   ubuntu@copious-alligator:~$ multipass exec copious-alligator -- less -f /var/log/syslog  # access logs of the machine (syslog)
   ```

   ![](.gitlab/img4.png)

## Task 2

CPU utilization & Memory swaps

![](.gitlab/img6.png)

Number of page faults

![](.gitlab/img7.png)

1. Test CPU

   ```bash
   for i in {1..10}; do sysbench --threads=2 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | 1           | 230.79            | 8.66             |
   | 2           | 227.78            | 8.78             |
   | 3           | 224.64            | 8.90             |
   | 4           | 224.45            | 8.91             |
   | 5           | 226.67            | 8.82             |
   | 6           | 213.62            | 9.36             |
   | 7           | 213.62            | 9.06             |
   | 8           | 218.94            | 9.13             |
   | 9           | 218.98            | 9.13             |
   | 10          | 219.60            | 9.10             |
   | **Average** | **222.616**       | **8.985**        |

   ```bash
   for i in {1..10}; do sysbench --threads=10 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | 1           | 444.93            | 22.46            |
   | 2           | 428.23            | 23.34            |
   | 3           | 403.49            | 24.77            |
   | 4           | 393.24            | 25.40            |
   | 5           | 399.29            | 25.02            |
   | 6           | 370.34            | 26.98            |
   | 7           | 391.58            | 25.52            |
   | 8           | 388.12            | 25.74            |
   | 9           | 395.41            | 25.25            |
   | 10          | 405.58            | 24.64            |
   | **Average** | **402.021**       | **24.912**       |

   ```bash
   for i in {1..10}; do sysbench --threads=100 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | 1           | 413.35            | 241.33           |
   | 2           | 402.68            | 247.51           |
   | 3           | 400.86            | 248.57           |
   | 4           | 400.55            | 249.04           |
   | 5           | 402.74            | 247.46           |
   | 6           | 405.94            | 245.56           |
   | 7           | 406.30            | 245.50           |
   | 8           | 402.99            | 247.50           |
   | 9           | 396.92            | 251.32           |
   | 10          | 392.88            | 253.93           |
   | **Average** | **402.521**       | **247.772**      |

2. Test scheduler performance, more specifically the cases when a scheduler has a large number of threads competing for some set of mutexes

   ```bash
   for i in {1..10}; sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run; done
   ```

   | Test №      | Total time (s) | Total number of events | Latency avg (ms) |
   | ----------- | -------------- | ---------------------- | ---------------- |
   | ...         | ...            |                        | ...              |
   | **Average** | **10.4118**    | **878**                | **834.61**       |

3. Multithread memory test: write

   ```bash
   for i in {1..10}; do sysbench --threads=2 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s) | Total number of events | Latency max (ms) |
   | ----------- | -------------- | ---------------------- | ---------------- |
   | ...         | ...            |                        | ...              |
   | **Average** | **14.8521**    | **104857600**          | **5.45**         |

   ```bash
   for i in {1..10}; do sysbench --threads=10 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s) | Total number of events | Latency max (ms) |
   | ----------- | -------------- | ---------------------- | ---------------- |
   | ...         | ...            |                        | ...              |
   | **Average** | **9.6059**     | **104857600**          | **37.37**        |

   ```bash
   for i in {1..10}; do sysbench --threads=100 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s) | Total number of events | Latency max (ms) |
   | ----------- | -------------- | ---------------------- | ---------------- |
   | ...         | ...            |                        | ...              |
   | **Average** | **9.1563**     | **104857600**          | **288.16**       |

4. Memory test

   ```bash
   sysbench --test=memory --memory-block-size=1M --memory-total-size=10G run
   ```

   | Test №      | Total time (s) | Total number of events | Latency max (ms) |
   | ----------- | -------------- | ---------------------- | ---------------- |
   | ...         | ...            |                        | ...              |
   | **Average** | **0.4754**     | **10240**              | **1.23**         |

5. Test I/O system

   ```bash
   sysbench --test=fileio --file-total-size=10G prepare
   sysbench --test=fileio --file-total-size=10G --file-test-mode=rndrw  --time=120 --max-time=300 --max-requests=0 run
   sysbench --test=fileio --file-total-size=10G cleanup
   ```

   | Test №                     | 1    | ...  | 10   | **Average** |
   | -------------------------- | ---- | ---- | ---- | ----------- |
   | File operations (reads/s)  | ...  | ...  | ...  | 331.52      |
   | File operations (writes/s) | ...  | ...  | ...  | 221.01      |
   | File operations (fsyncs/s) | ...  | ...  | ...  | 707.60      |
   | Throughput read (MiB/s)    | ...  | ...  | ...  | 5.18        |
   | Throughput written (MiB/s) | ...  | ...  | ...  | 3.45        |

## Task 4

Switch to `libvirt`: https://multipass.run/docs/set-up-the-driver#heading--linux-use-libvirt

1. Test CPU

   ```bash
   for i in {1..10}; do sysbench --threads=2 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | ...         | ...               | ...              |
   | **Average** | **228** (+6)      | **8.77** (-0.2)  |

   ```bash
   for i in {1..10}; do sysbench --threads=10 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | ...         | ...               | ...              |
   | **Average** | **421** (+19)     | **23.68** (-1.2) |

   ```bash
   for i in {1..10}; do sysbench --threads=100 --time=60 cpu --cpu-max-prime=64000 run; done
   ```

   | Test №      | Events per second | Latency avg (ms) |
   | ----------- | ----------------- | ---------------- |
   | ...         | ...               | ...              |
   | **Average** | **419** (+9)      | **241.63** (-6)  |

2. Test scheduler performance, more specifically the cases when a scheduler has a large number of threads competing for some set of mutexes

   ```bash
   for i in {1..10}; sysbench --num-threads=64 --test=threads --thread-yields=100 --thread-locks=2 run; done
   ```

   | Test №      | Total time (s)     | Total number of events | Latency avg (ms)   |
   | ----------- | ------------------ | ---------------------- | ------------------ |
   | ...         | ...                |                        | ...                |
   | **Average** | **10.388** (-0.02) | **824** (-54)          | **794.76** (-39.8) |

3. Multithread memory test: write

   ```bash
   for i in {1..10}; do sysbench --threads=2 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s)      | Total number of events | Latency max (ms) |
   | ----------- | ------------------- | ---------------------- | ---------------- |
   | ...         | ...                 |                        | ...              |
   | **Average** | **14.0146** (-0.83) | **104857600**          | **6.18**         |

   ```bash
   for i in {1..10}; do sysbench --threads=10 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s)     | Total number of events | Latency max (ms) |
   | ----------- | ------------------ | ---------------------- | ---------------- |
   | ...         | ...                |                        | ...              |
   | **Average** | **9.4546** (-0.15) | **104857600**          | **37.25**        |

   ```bash
   for i in {1..10}; do sysbench --threads=100 --time=60 memory --memory-oper=write run; done
   ```

   | Test №      | Total time (s)     | Total number of events | Latency max (ms) |
   | ----------- | ------------------ | ---------------------- | ---------------- |
   | ...         | ...                |                        | ...              |
   | **Average** | **8.7958** (-0.36) | **104857600**          | **288.15**       |

4. Memory test

   ```bash
   sysbench --test=memory --memory-block-size=1M --memory-total-size=10G run
   ```

   | Test №      | Total time (s)       | Total number of events | Latency max (ms) |
   | ----------- | -------------------- | ---------------------- | ---------------- |
   | ...         | ...                  |                        | ...              |
   | **Average** | **0.4339** (-0.0415) | **10240**              | **3.45** (+2.2)  |

5. Test I/O system

   ```bash
   sysbench --test=fileio --file-total-size=10G prepare
   sysbench --test=fileio --file-total-size=10G --file-test-mode=rndrw  --time=120 --max-time=300 --max-requests=0 run
   sysbench --test=fileio --file-total-size=10G cleanup
   ```

   | Test №                     | 1    | ...  | 10   | **Average**     |
   | -------------------------- | ---- | ---- | ---- | --------------- |
   | File operations (reads/s)  | ...  | ...  | ...  | 307.06 (-24.46) |
   | File operations (writes/s) | ...  | ...  | ...  | 204.71 (-16.3)  |
   | File operations (fsyncs/s) | ...  | ...  | ...  | 655.48 (-52.12) |
   | Throughput read (MiB/s)    | ...  | ...  | ...  | 4.8 (-0.38)     |
   | Throughput written (MiB/s) | ...  | ...  | ...  | 3.2 (-0.25)     |

**What differences do you see in the results you collected?**

Overall, the performance has become slightly better. However, I/O performance has become slightly worse. No significant changes in performance.
